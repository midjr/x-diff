(defproject x-diff "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.xml "0.0.8"]]
  :main ^:skip-aot x-diff.core
  :target-path "target/%s"
  :aliases { "bikeshed" ["bikeshed" "--max-line-length" "120" "--var-redefs" "false"]
             "lint" ["do" ["eastwood"] "bikeshed" "docstring-checker"]}
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[jonase/eastwood "0.2.5" :exclusions [org.clojure/clojure]]
                             [lein-bikeshed "0.5.1"]
                             [docstring-checker "1.0.2"]]
                    :eastwood {:add-linters [:unused-locals
                                             :unused-private-vars]
                               :exclude-namespaces [:test-paths]}}}
  :docstring-checker {:include [#"^x-diff"]
                      :exclude [#"^test"]})
